<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ExampleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //
    public function facebook(Request $request)
    {
        $token = $request->input('token');

        $url = 'https://graph.facebook.com/v2.7/me?fields=id,name,picture&access_token='.$token;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch,CURLOPT_URL,$url);
        $result = curl_exec($ch);
        curl_close($ch);

        $result = json_decode($result);
        // echo $result->name;
        return response()->json([
            'status'=>200,
            'data'=>[
                'nama'=>$result->name,
                'picture'=>$result->picture->data->url
            ]
        ]);
    }
}
